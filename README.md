# README #

This ReadME details the steps to be taken in order to test usage of Ansible playbook to check for archived RDS MySQL tables, backing them to AWS S3 and alerting using AWS SNS.
### What is this repository for? ###

This repository contains two files

aws_ansible.yml	-	This file is the playbook which contains the tasks required to accomplish this project. Details will be provided in the 
					next section
variables.yml	-	This file contains variables required for the execution of the aws_ansible.yml playbook

### How do I get set up? ###

* Summary of set up

The services below have been used for setting up this project. The parameters used have also been highlighted

AMAZON RDS MYSQL Setup
The RDS MYSQL setup was done using the following parameters

InstanceName		= 	josh-ansible-db01
DatabaseName		= 	joshansible
Username			= 	josh_ansible
DatabasePassword	= 	#########{found in the variables.yml file}
TableNames			=	tala_users, tala_department, ARCHIVE_tala_users, ARCHIVE_tala_department

S3 Setup
The S3 Bucket has been configured with the following parameters
Bucket				=	joshansibleaws
*Public access*

SNS Setup
TopicARN			=	arn:aws:sns:eu-west-2:097408484268:josh_ansible_aws_mail_notification
Protocol			=	Email
Email				=	joshuamangi@gmail.com
DisplayName			=	AWSTala

Amazon EC2
VirtualMachine		=	app-josh-ansible-02
OperatingSystem		=	Amazon Linux AMI
Size				=	t2

* Configuration

The Amazon MYSQL RDS, Amazon S3, Amazon EC2 and Amazon SNS services have been configured using the Amazon Console with
the parameters shown above.

EC2 Configuration
Login to AWS Console and launch an Amazon Linux AMI instance of size t2 in a security group. This EC2 instance was configured using
the parameters contained in the EC2 section above. This is where the ansible server is located.

SNS Configuration
Login to AWS Console and launch the SNS service using the configuration parameters shown in the SNS Setup section above.
This SNS instance is using my email address {joshuamangi@gmail.com} to receive emails successful/failed notifications

S3 Configuration
Login to AWS Console and create an S3 instance with the confugration parameters shown in the S3 Setup section above.
It is a publicly accessible and while doing tests the URL to the S3 instance can be reached to ascertain backup has been done.

* Dependencies

The following are depencies which must be available for successful tests to be done

mysql				=	install mysql. 
						This instance uses the yum package hence this command was issued yum -y install mysql.
						MySQL is used to connect to the MYSQL RDS instance to create, delete, backup on the shell in ansible
aws cli				=	install aws.
						AWS CLI is used to connect to the S3 and SNS Services.
						Issue this command to install aws cli pip install awscli --upgrade --user
git					=	install git.
						Git is used to connect to bitbucket to clone, push and pull purposes
						Issue this command yum -y install git
ansible				=	install ansible
						Ansible must be present so as to be able to execute the playbook to achieve the purposes of this
						repository
						Issue this command  yum -y install ansible

* Database configuration

The tables in the database instance in MYSQL RDS have been created using the queries below.

insert into joshansible.tala_users (idtala_users,tala_firstname,tala_lastname) VALUES (1,'Joshua','Mangi');
insert into joshansible.tala_users (idtala_users,tala_firstname,tala_lastname) VALUES (2,'Jay','Z');
insert into joshansible.tala_users (idtala_users,tala_firstname,tala_lastname) VALUES (3,'Marshall','Mathers');

insert into joshansible.tala_department (idtala_department,department_name) VALUES (1,'Tech');
insert into joshansible.tala_department (idtala_department,department_name) VALUES (2,'Finance');
insert into joshansible.tala_department (idtala_department,department_name) VALUES (3,'Data');

create table joshansible.ARCHIVE_tala_users LIKE joshansible.tala_users;
create table joshansible.ARCHIVE_joshansibletala_department LIKE joshansible.tala_department;

insert into joshansible.ARCHIVE_tala_users select * from joshansible.tala_users;
insert into joshansible.ARCHIVE_joshansibletala_department select * from joshansible.tala_department;

select * from joshansible.ARCHIVE_tala_users;
select * from joshansible.ARCHIVE_joshansibletala_department;

* How to run tests

To run the tests traverse through the steps below 
Step 1 
use the public key below to connect to the EC2 Instance:

-----BEGIN RSA PRIVATE KEY-----
MIIEpAIBAAKCAQEArCkhEWmQVuSl8gBxGO/qzlLf8XDyqbMbjrncEZ4hrGYzj0SD9kS0tBZ1dE8X
H7uQU0kP/Ak1iBujla/y1cfzoF0bxlNSDr/zvVG/3lND+rwMVm6L2ZWT09j2eqDFTS6DVTY0gkbJ
C1xGSGOASezlmCnWEQFfcIe+zYIkNjFSIWoaFSAUuMXO6ZG6kmEU4WrY/mrjvuJ///VDh5DA4mKE
feCfYBFK7FQ0m/0hP4aLaB4ZlfMG0N99PXCsnNw+PCrdHFiq+XUATcVFDDV+BZ0Q2haJaBGSHkV2
Q/lgcn70U3qOTeXa//8AXWC0HH7AQ9XdLKd/tMzJQWCqplCu18xkIQIDAQABAoIBACLfVeZsBqPD
PuXtNvuBJYPTf68tXMoJEKzxcuVSral5Ljp3+2737c6IuhPsLVftK0fjhcxcpS5DwvgAIuUw0mWK
VqTunbGFKj8P/xhur1drnegoDkXtqXsA+LRQOobuxAWPrEUzR2f4sffZx9Oxm7QdzVG2gHmP1ZNV
KDad+O/xc5wV9bwn98RTaQcINrf8BBEaFVvcC1TFh5qjP2uWFag4c5W2P9aDGt9MGH/M0u4y6uE9
fdjRsobSFMUH/wNUD7Cp5rGZ1juRMlv5xfLz38cfTBq+rURSOhpedBcjoTZUuDVTyLmvQEhyfpsw
P4glyQemXj6ybXkDgtmKYYVzqEECgYEA5i4IQBbgte+2IVg4/TCeN5QOJZyzQn2GoFeoGNjPln3D
qqWNkuGQWYpuCQjM8QTXN0z2qCus4mCQgLEZGcDqfxLSkWWJMQ1nGWcpjex2s2UP6piN/7wx+aQY
P4BZ66YH17qgxR1usKfqX3VyCtQxhY2pmMNT0gbz4dtFscUeS3kCgYEAv3j81f/Vtcp0Er+tS2Ng
sea+We7cbfSUPBb1K7vKVDiTXaHkPvDZFJYLDvWTgJqqHqlSyAJUp4q7WKiUfiXDLEARYk8L4fgB
LsP0+L4wXAyoS89TSJT1QpEsin9A+F0cWO3x8xZD/AU+9tcW/iFrH0OEs68j/ts349Qt5F93i+kC
gYEAy3ZhIJWYZ/nOGkdriJKTKDmdD64l3l4qxEz3b48mX7YnYQ4ZyH3OJPfyL9YxodJPkrhbR2fj
1+7XjIn+z/7wyQaQTqlbzZo1SgUgfC1K92Qb35id3DI0UqC0Nl10HpKSWCTFu7c6grjKuTqNZfkP
FSiVg00VKI+wwDIctviwwLECgYBrom63OfcAF4wCkh+vtnC/8Q9mbOEbKpc95EX0TL+jcp5cz0oI
m8ncSbBzu1csv0UMsXCooK7H9DP21eWLuFyr/mQHWJo8b/byPBP01K1IwZDF9QlDTwFEZ2mbfYef
VnA+CZyuYwDapnRLvIPfXhfi3ki13amldcRIXZlivMvuKQKBgQDIuDx/CziVeLAUpk9ZQOAoms8R
wdjOK5ZxmGwvg2yaPL4+Fqxofe4TbVouFm8NhDd3eN1N2dG4kss86JXWjPhf8vhdburQc+psteUO
TalYpLkiXP5jkvTyYBLqzEZwYlUzFrECUhXsq18A5SmDjInr50uL+Pt1H+5X7Vr65XQRDg==
-----END RSA PRIVATE KEY-----

Copy the above public key and save it in a .pem file within the computer you
 are in eg {talaaccess.pem}
After the file has been saved use the command below to connect to the EC2 instance 
* remember to point the ssh command below to the location of the .pem file

ssh -i talaaccess.pem talaaccess@35.176.223.197

Step 2
Once log in to the box is successful use the following commanc to clone the repository

git clone https://joshua_mangi@bitbucket.org/joshua_mangi/josh_tala_ansible_aws_integration.git

This will clone the repository in the home folder/location where you are

Step 3
Change directory to the josh_tala_ansible_aws_integration folder using the command below
cd josh_tala_ansible_aws_integration

Step 4
Execute the ansible command below to execute the playbook 

ansible-playbook -i "localhost," -c local aws_ansible.yml

The output should show successful tasks being executed
* Deployment instructions

The deployments have already been done in AWS
